# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.middleware.cache import FetchFromCacheMiddleware \
    as DjangoFetchFromCacheMiddleware


class FetchFromCacheMiddleware(DjangoFetchFromCacheMiddleware):
    """An extended verison of Django's FetchFromCacheMiddleware, which skips
    fetching from the cache if the request has the ``_cache_skip_cache``
    attribute set."""

    def process_request(self, request, *args, **kwargs):
        """Checks whether the request has been flagged to skip fetching from
        the cache."""

        if getattr(request, '_cache_skip_cache', False):
            request._cache_update_cache = False  # Don't update the cache.
            return None  # Don't check the cache.

        return super(FetchFromCacheMiddleware, self).process_request(
            request, *args, **kwargs)


class BaseSkipCacheMiddleware(object):

    def _should_skip_cache(self, request):
        raise NotImplementedError('This method should be overridden.')

    def process_request(self, request, *args, **kwargs):
        if self._should_skip_cache(request):
            request._cache_skip_cache = True


class AuthenticatedSkipCacheMiddleware(BaseSkipCacheMiddleware):
    """Set the ``_cache_skip_cache`` attribute on the request if the user is
    authenticated."""

    def _should_skip_cache(self, request):
        return request.user.is_authenticated()


class StaffSkipCacheMiddleware(BaseSkipCacheMiddleware):
    """Set the ``_cache_skip_cache`` attribute on the request if the user is a
    staff member."""

    def _should_skip_cache(self, request):
        return request.user.is_authenticated() and request.user.is_staff
